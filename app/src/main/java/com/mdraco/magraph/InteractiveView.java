package com.mdraco.magraph;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.Random;

/**
 * Created by mateusz on 12.06.2014.
 *
 * View that is meant to be interactive - you may draw onto it, clear it etc.
 * All drawing is stored in bitmap so it should be easy to save or load it.
 */
public class InteractiveView extends View {
    private Bitmap bitmap;
    private Canvas canvas;
    private Paint paint;
    private float _x;
    private float _y;

    public InteractiveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    /**
     * when size change we want to recreate the bitmap
     * @param w new width
     * @param h new height
     * @param oldw old width
     * @param oldh old height
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);

        // draw a red circle on the center (just for fun)
        int r = Math.min(w, h) * 2 / 3;
        int x = w / 2;
        int y = h / 2;

        paint.setColor(Color.RED);
        canvas.drawCircle(x, y, r / 2, paint);
    }

    /**
     * All we do here is to draw bitmap onto screen.
     * @param canvas screen canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        if (bitmap != null)
            canvas.drawBitmap(this.bitmap, 0, 0, paint);
    }

    /**
     * Touch event. Both drawing and multitouch.
     * @param event object with touch event data
     * @return true if we want to keep tracking event
     */
    @SuppressWarnings("NullableProblems")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // on multitouch we are clear the screen
        if (event.getPointerCount() > 1) {
            int action = event.getActionMasked();
            if (action == MotionEvent.ACTION_POINTER_DOWN) {
                // we want white to be background more frequent than other colours
                if (random.nextInt(3) == 0)
                    canvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);
                else
                    canvas.drawColor(paint.getColor());
                this.invalidate();
            }
            // ignore all future events of multitouch
            return false;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // store position
                _x = event.getX();
                _y = event.getY();
                // select random color on finger down
                paint.setColor(getRandomColor());
                // yes, we want to track position
                return true;
            case MotionEvent.ACTION_MOVE:
                // draw line
                canvas.drawLine(_x, _y, event.getX(), event.getY(), paint);
                // store position
                _x = event.getX();
                _y = event.getY();
                // invalidate (and draw onto screen)
                this.invalidate();
                // keep tracking position
                return true;
        }
        return super.onTouchEvent(event);
    }

    /**
     * available random colours
     */
    private final int[] colors = new int[] {
            Color.RED,
            Color.BLUE,
            Color.YELLOW,
            Color.BLACK,
            Color.GREEN,
            Color.GRAY,
            Color.BLACK,
    };
    private final Random random = new Random();

    /**
     * select random colour
     * @return random colour
     */
    private int getRandomColor() {
        return colors[random.nextInt(colors.length)];
    }
}
